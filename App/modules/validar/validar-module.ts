module Validar {
    'use strict';

    import Paths = Constants.Paths;
    let Page = Paths.Validar;

    angular.module(Page.Base, [])
        .config(statesConfiguration);

    function statesConfiguration(
        $stateProvider: ng.ui.IStateProvider
    ): void {

        $stateProvider
            .state(Paths.Tabs + '.' + Page.Base, {
                url: '/' + Page.Base,
                views: {
                    'validar': {
                        templateUrl: Paths.Modules + 'validar/views/validar.html',
                    }
                }
            }
        );
    }
}
