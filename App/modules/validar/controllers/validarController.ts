﻿module Validar {
    'use strict';

    export class ValidarController {

        private text: string = 'hola';

        constructor(
            private loadingService: Core.ILoadingService
        ) {
            this.addTextAsync();
        }


        private addTextAsync(): void {
            this.loadingService.show();
            window.setTimeout(() => {
                this.text += '<p>Lorem ip pulvinar ac nibh ac lobortis.</p>';
            this.loadingService.hide();
            }, Math.floor(Math.random() * 3000));
        }
    }

    angular.module(Constants.Paths.Validar.Base)
        .controller('validarController', ValidarController);
}
